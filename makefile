.PHONY: downall
downall:
	docker-compose -f docker-compose-bots.yml -f docker-compose.yml down

.PHONY: upall
upall:
	docker-compose -f docker-compose-bots.yml -f docker-compose.yml up -d

.PHONY: logsapi
logsapi:
	docker-compose logs -f api-bot

.PHONY: upbots
upbots:
	docker-compose -f docker-compose-bots.yml up -d

.PHONY: upserver
upserver:
	docker-compose up -d