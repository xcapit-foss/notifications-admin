# README - notifications-admin

# Xcapit Notification Service

Xcapit Notification Service is an api rest service in charge of sending notifications to users by various means such as email, push notifications and telegram.

## Community

- [Discord](https://discord.gg/AnGXcZ8P)
- [Project Charter](https://xcapit-foss.gitlab.io/documentation/docs/project_charter)
- [Code of Conduct](https://xcapit-foss.gitlab.io/documentation/docs/CODE_OF_CONDUCT)
- [Contribution Guideline](https://xcapit-foss.gitlab.io/documentation/docs/contribution_guidelines)

## Getting Started

[Getting started](https://xcapit-foss.gitlab.io/documentation/docs/notifications-service/getting_started) 
