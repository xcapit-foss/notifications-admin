FROM python:3.8

ENV PYTHONUNBUFFERED 1

WORKDIR /code
COPY wrapperbotadmin .
RUN pip install --no-cache-dir --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt
