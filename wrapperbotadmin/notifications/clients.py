from abc import ABC

import requests
from requests import Response

from wrapperbotadmin.settings import EMAIL_FROM_USER, API_URL, API_APP_URL
import logging
from notifications.emails import EmailSender


logger = logging.getLogger(__name__)


class DjangoEmailClient:
    EMAIL_VALIDATION_SUBJECT = '[XCAPIT] Verificá tu email'
    REFERRAL_EMAIL_SUBJECT = '[XCAPIT] Te han referido!'
    EMAIL_PASSWORD_RESET_SUBJECT = '[XCAPIT] Restablecer la contraseña'

    def send_email_validation(self, data: dict):
        email_sender = EmailSender(
            subject=self.EMAIL_VALIDATION_SUBJECT,
            text_template_name='email_validation.txt',
            html_template_name='email_validation.html',
            from_email=EMAIL_FROM_USER,
            to=[data['email']],
            context=data
        )
        email_sender.execute()

    def send_referral_email(self, data: dict):
        email_sender = EmailSender(
            subject=self.REFERRAL_EMAIL_SUBJECT,
            text_template_name='referral_email.txt',
            html_template_name='referral_email.html',
            from_email=EMAIL_FROM_USER,
            to=[data['referral_email']],
            context=data
        )
        email_sender.execute()

    def send_reset_password_email(self, data: dict):
        email_sender = EmailSender(
            subject=self.EMAIL_PASSWORD_RESET_SUBJECT,
            text_template_name='reset_password_email.txt',
            html_template_name='reset_password_email.html',
            from_email=EMAIL_FROM_USER,
            to=[data['email']],
            context=data
        )
        email_sender.execute()

    @staticmethod
    def _fix_percentage_decimals(data):
        data['percentage'] = f"{float(data['percentage']) :.2f}"
        return data

    def send_finish_fund_email(self, data: dict):
        if float(data['percentage']) < 0:
            html_template_name = 'notification_SL.html'
            text_template_name = 'notification_SL.txt'
        else:
            html_template_name = 'notification_TP.html'
            text_template_name = 'notification_TP.txt'

        data = self._fix_percentage_decimals(data)

        email_sender = EmailSender(
            subject=f'[XCAPIT] Salio el fondo {data["nombre_bot"]}',
            text_template_name=text_template_name,
            html_template_name=html_template_name,
            from_email=EMAIL_FROM_USER,
            to=[data['email']],
            context=data
        )
        email_sender.execute()


class Client(ABC):
    api_url = ''

    def generate_endpoint(self, path: str):
        return f'{self.api_url}{path}'

    @staticmethod
    def status_ok(response: Response):
        return response.status_code == 200

    def return_json_if_status_ok(self, response: Response):
        return response.json() if self.status_ok(response) else None


class ApiBotClient(Client):
    api_url = API_URL

    def get_first_subscriber(self, fund_name: str):
        url = self.generate_endpoint(f'suscriptores/primerSuscriptor/fundname/{fund_name}')
        response = requests.get(url)
        return self.return_json_if_status_ok(response)


class ApiAppClient(Client):
    api_url = API_APP_URL

    def get_user(self, id_user: str):
        url = self.generate_endpoint(f'users/{id_user}')
        response = requests.get(url)
        return self.return_json_if_status_ok(response)

