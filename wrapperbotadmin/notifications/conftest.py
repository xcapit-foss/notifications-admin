import pytest
from rest_framework.reverse import reverse
from push_notifications.models import GCMDevice


@pytest.fixture
def validation_data():
    return {
        'email': 'test@test.com',
        'uid': 'testuid',
        'token': 'sometoken',
        'domain': 'https://sometestdomain.com'
    }


@pytest.fixture
def referral_data():
    return {
        'referral_email': 'test@test.com',
        'encode_email': 'bWFydGluYnJ1bm9yaTMxNkBnbWFpbC5jb20',
        'user_email': 'test@xcapit.com',
        'referral_code': 'ap5RHX',
        'domain': 'http://localhost:8100'
    }


@pytest.fixture
def post_notification(client):
    def p(url, data, reverse_kargs=()):
        return client.post(f'{reverse(url, kwargs=reverse_kargs)}', data=data)

    return p


@pytest.fixture
def finish_fund_data():
    return {
        'nombre_bot': 'test',
        'message': 'Some message',
        'id_user': '20',
        'id_chat': '1213123123',
        'email': 'test@test.com',
        'percentage': '20.52'
    }


@pytest.fixture
def finish_fund_data_negative():
    return {
        'nombre_bot': 'test',
        'message': 'Some message',
        'id_user': '20',
        'id_chat': '1213123123',
        'email': 'test@test.com',
        'percentage': -20.2
    }


@pytest.fixture
def user_gcm_devices():
    def ugd(user_id):
        GCMDevice.objects.create(name=user_id, active=True, registration_id='test1')
        GCMDevice.objects.create(name=user_id, active=False, registration_id='test2')
        GCMDevice.objects.create(name=user_id, active=True, registration_id='test3')
        GCMDevice.objects.create(name=user_id, active=False, registration_id='test4')

    return ugd
