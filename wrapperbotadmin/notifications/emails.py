from django.template.loader import render_to_string
from django.core.mail import EmailMultiAlternatives
from typing import List


class EmailSender:
    def __init__(
            self,
            subject: str,
            text_template_name: str,
            html_template_name: str,
            from_email: str,
            to: List[str],
            context: dict
    ):
        self._subject = subject
        self._text_message = render_to_string(text_template_name, context)
        self._html_message = render_to_string(html_template_name, context)
        self._from_email = from_email
        self._to = to
        self._context = context
        self._email = None

    def _create_email(self):
        self._email = EmailMultiAlternatives(
            subject=self._subject,
            body=self._text_message,
            from_email=self._from_email,
            to=self._to
        )

    def _attach_html_to_email(self):
        self._email.attach_alternative(self._html_message, 'text/html')

    def _send(self):
        self._email.send(fail_silently=False)

    def execute(self):
        self._create_email()
        self._attach_html_to_email()
        self._send()
