import pytest
from unittest.mock import patch
from notifications.helpers import send_telegram_msg


@patch('requests.post')
def test_send_telegram_msg_ok(post_mock):
    post_mock.return_value.status_code = 200
    send_telegram_msg('asdf', 'asd', 'fasd')
    post_mock.assert_called()


@patch('requests.post')
def test_send_telegram_msg_not_ok(post_mock):
    post_mock.return_value.status_code = 400
    with pytest.raises(Exception):
        send_telegram_msg('asdf', 'asd', 'fasd')

    post_mock.assert_called()
