from unittest.mock import Mock
from notifications.serializers import SubscriberNotificationSerializer, EmailValidationSerializer, \
    ReferralEmailSerializer, NotificationsSerializer
import pytest


@pytest.mark.parametrize(
    'serializer,data',
    [
        (
                SubscriberNotificationSerializer, {
                    'message': 'test',
                    'nivel_de_riesgo': 'volume_profile_strategies_USDT'
                }
        ),
        (
                EmailValidationSerializer, {
                    'email': 'test@test.com',
                    'uid': 'testuid',
                    'token': 'sometoken',
                    'domain': 'https://sometestdomain.com'
                }
        ),
        (
                ReferralEmailSerializer, {
                    'referral_email': 'test@test.com',
                    'encode_email': 'bWFydGluYnJ1bm9yaTMxNkBnbWFpbC5jb20',
                    'user_email': 'test@xcapit.com',
                    'referral_code': 'ap5RHX',
                    'domain': 'http://localhost:8100'
                }
        )
    ]
)
def test_serializers(serializer, data):
    ser = serializer(data=data)
    assert ser.is_valid()
    assert None is ser.create(ser.validated_data)
    assert None is ser.update(Mock(), ser.validated_data)


@pytest.mark.django_db
def test_notifications_serializer_update():
    ser = NotificationsSerializer(data={
        "user_id": "123",
        "type": "sl",
        "title": "test",
        "message": "test",
        "read": "False"
    }
    )
    assert ser.is_valid()
    assert ser.create(ser.validated_data) is not None
    assert ser.update(Mock(), ser.validated_data) is not None

