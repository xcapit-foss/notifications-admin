from datetime import datetime
from unittest.mock import patch, Mock
import pytest
import pytz
from rest_framework import status
from rest_framework.reverse import reverse
from notifications.views import Administrative, Dev, Subs, ERROR_MSG_RESPONSE
from notifications.models import Notifications
from notifications.helpers import MapRiskLevels
from push_notifications.models import GCMDevice

TEST_MSG = {'message': 'test'}
TEST_MSG1 = {'message': 'test', 'nivel_de_riesgo': 'volume_profile_strategies_USDT'}


@patch('notifications.async_email_sender.send_validation_email.apply_async')
def test_send_validation_email(apply_async_mock, client, validation_data):
    apply_async_mock.return_value = None
    url = reverse('notifications:send-email-validation')
    response = client.post(url, data=validation_data)
    assert response.status_code == status.HTTP_200_OK
    apply_async_mock.assert_called()


@patch('notifications.async_email_sender.send_validation_email.apply_async')
def test_send_validation_email_invalid_data(apply_async_mock, client):
    apply_async_mock.return_value = None
    url = reverse('notifications:send-email-validation')
    response = client.post(url)
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    apply_async_mock.assert_not_called()


@patch('notifications.async_email_sender.send_referral_email.apply_async')
def test_send_referral_email(apply_async_mock, client, referral_data):
    apply_async_mock.return_value = None
    url = reverse('notifications:send-referral-email')
    response = client.post(url, data=referral_data)
    assert response.status_code == status.HTTP_200_OK
    apply_async_mock.assert_called()


@patch('notifications.async_email_sender.send_referral_email.apply_async')
def test_send_referral_email_invalid(apply_async_mock, client, referral_data):
    apply_async_mock.return_value = None
    url = reverse('notifications:send-referral-email')
    referral_data_invalid = referral_data.copy()
    referral_data_invalid.pop('referral_email')
    response = client.post(url, data=referral_data_invalid)
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    apply_async_mock.assert_not_called()


@patch('notifications.async_email_sender.send_reset_password_email.apply_async')
def test_send_email_reset_password(apply_async_mock, client, validation_data):
    apply_async_mock.return_value = None
    url = reverse('notifications:send-email-reset-password')
    response = client.post(url, data=validation_data)
    assert response.status_code == status.HTTP_200_OK
    apply_async_mock.assert_called()


@patch('notifications.async_email_sender.send_reset_password_email.apply_async')
def test_send_email_reset_password_invalid(apply_async_mock, client, validation_data):
    apply_async_mock.return_value = None
    url = reverse('notifications:send-email-reset-password')
    validation_data_invalid = validation_data.copy()
    validation_data_invalid.pop('email')
    response = client.post(url, data=validation_data_invalid)
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    apply_async_mock.assert_not_called()


@pytest.mark.django_db
@patch('notifications.views.PushNotification')
@patch('requests.get')
def test_create_user_notification(get_mock, mock_push_notification, post_notification):
    data = [
        {
            "user_id": "123",
            "type": "sl",
            "title": "test",
            "message": "test",
            "read": "False"
        }
    ]

    get_mock.return_value.json.return_value = {'id_chat': 1234, 'id_user': 123}
    get_mock.return_value.status_code = 200

    response = post_notification('notifications:create-notification', data[0])

    get_mock.assert_called()
    assert response.status_code == status.HTTP_201_CREATED


@pytest.mark.django_db
def test_count_user_notifications(client):
    notifications = [
        {
            "id": "1",
            "created": "2020-11-15 10:51:15.00+00",
            "user_id": "123",
            "type": "sl",
            "title": "test",
            "message": "test",
            "read": "False"
        },
        {
            "id": "2",
            "created": "2020-11-15 12:51:15.00+00",
            "user_id": "123",
            "type": "sl",
            "title": "test 2",
            "message": "test 2",
            "read": "False"
        }
    ]

    user_id = 123
    mock_response = {'count': 2}

    Notifications.objects.create(**notifications[0])
    Notifications.objects.create(**notifications[1])

    url = reverse('notifications:count-notifications', kwargs={'user_id': user_id})
    response = client.get(url)

    assert response.status_code == status.HTTP_200_OK
    assert response.json() == mock_response


@pytest.mark.django_db
def test_mark_as_read_user_notifications_without_false_values(client):
    notifications = [
        {
            "created": "2020-11-15T12:51:15Z",
            "read": "True",
            "user_id": "123",
            "type": "sl",
            "title": "test 2",
            "message": "test 2"
        }
    ]

    user_id = 123
    fake_response = {'status': 'No hay registros a actualizar'}

    Notifications.objects.create(**notifications[0])

    url = reverse('notifications:user-notifications', kwargs={'user_id': user_id})
    response = client.put(url)

    assert response.status_code == status.HTTP_200_OK
    assert response.json() == fake_response


@pytest.mark.django_db
def test_mark_as_read_user_notifications(client):
    notifications = [
        {
            "created": "2020-11-15T12:51:15Z",
            "read": "False",
            "user_id": "123",
            "type": "sl",
            "title": "test 2",
            "message": "test 2"
        },
        {
            "created": "2020-11-15T10:51:15Z",
            "read": "False",
            "user_id": "123",
            "type": "sl",
            "title": "test",
            "message": "test"
        }
    ]

    user_id = 123

    Notifications.objects.create(**notifications[0])
    Notifications.objects.create(**notifications[1])

    url = reverse('notifications:user-notifications', kwargs={'user_id': user_id})
    response = client.put(url)

    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
@patch('django.utils.timezone.now')
def test_get_user_notifications(mock_timezone, client):
    notifications = [
        {
            "created": "2020-11-15T12:51:15Z",
            "read": "False",
            "user_id": "123",
            "type": "sl",
            "title": "test 2",
            "message": "test 2"
        },
        {
            "created": "2020-11-15T10:51:15Z",
            "read": "False",
            "user_id": "123",
            "type": "sl",
            "title": "test",
            "message": "test"
        }
    ]

    user_id = 123

    date = datetime(2020, 11, 15, 12, 51, 15, tzinfo=pytz.UTC)
    mock_timezone.return_value = date
    Notifications.objects.create(**notifications[0])
    date = datetime(2020, 11, 15, 10, 51, 15, tzinfo=pytz.UTC)
    mock_timezone.return_value = date
    Notifications.objects.create(**notifications[1])

    url = reverse('notifications:user-notifications', kwargs={'user_id': user_id})
    response = client.get(url)

    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()) == 2


def get_args_number(call_args_list):
    return len(list(call_args_list[0])[0])


@pytest.mark.parametrize('url_id, send_msg_mock, expected_value', [
    ['notifications:dev_notification', 'notifications.views.Dev.send_msg', Dev.OK_MESSAGE_RESPONSE],
    ['notifications:admin_notification', 'notifications.views.Administrative.send_msg',
     Administrative.OK_MESSAGE_RESPONSE],
])
def test_notifications_admin_and_dev_ok(monkeypatch, post_notification, url_id, send_msg_mock, expected_value):
    with patch(send_msg_mock) as send_msg:
        response = post_notification(url_id, TEST_MSG)

        assert response.json() == expected_value
        send_msg.assert_called()
        assert get_args_number(send_msg.call_args_list) == 3


@pytest.mark.parametrize('url_id, send_msg_mock, reverse_kargs', [
    ['notifications:dev_notification', 'notifications.views.Dev.send_msg', {}],
    ['notifications:admin_notification', 'notifications.views.Administrative.send_msg', {}],
    ['notifications:subscriptor_notification', 'notifications.services.send_telegram_msg', {'fund_name': 'test'}]
])
def test_notifications_admin_and_dev_and_subs_without_message(post_notification, url_id, send_msg_mock, reverse_kargs):
    with patch(send_msg_mock) as send_msg:
        response = post_notification(url_id, {}, reverse_kargs=reverse_kargs)
        assert response.json() == ERROR_MSG_RESPONSE
        send_msg.assert_not_called()


@patch('requests.get')
@patch('notifications.services.send_telegram_msg')
def test_notifications_subscribers_ok(send_msg_mock, get_mock, post_notification):
    send_msg_mock.return_value = None
    map_risk_levels = MapRiskLevels()
    fund_name = 'test'
    get_mock.return_value.json.return_value = {'id_chat': '1234', 'id_user': None}
    get_mock.return_value.status_code = 200
    response = post_notification(
        'notifications:subscriptor_notification',
        TEST_MSG1,
        {'fund_name': fund_name}
    )

    get_mock.assert_called()
    assert response.json() == 'todo bien'
    send_msg_mock.assert_called()
    assert get_args_number(send_msg_mock.call_args_list) == 3


@patch('requests.get')
@patch('notifications.services.send_telegram_msg')
@patch('notifications.async_email_sender.send_finish_fund_email.apply_async')
def test_notifications_subscribers_ok_with_id_user(apply_async_mock, send_msg_mock, get_mock, post_notification):
    apply_async_mock.return_value = None
    fund_name = 'test'
    fake_responses_get = [Mock(), Mock()]

    fake_responses_get[0].json.return_value = {'id_chat': '1234', 'id_user': '1234'}
    fake_responses_get[0].status_code = 200

    fake_responses_get[1].json.return_value = {'email': 'test@test.com'}
    fake_responses_get[1].status_code = 200

    get_mock.side_effect = fake_responses_get
    test_msg = TEST_MSG.copy()
    test_msg['percentage'] = 10.0
    test_msg['nombre_bot'] = 'Test'
    test_msg['cantidad_dias'] = 61
    test_msg['fecha_inicio'] = '15/01/2020 14:55:00'
    test_msg['nivel_de_riesgo'] = 'volume_profile_strategies_USDT'
    test_msg['perdida'] = 15
    test_msg['ganancia'] = 20

    response = post_notification(
        'notifications:subscriptor_notification',
        test_msg,
        {'fund_name': fund_name})

    get_mock.assert_called()
    assert response.json() == Subs.OK_MESSAGE_RESPONSE
    send_msg_mock.assert_called()
    assert get_args_number(send_msg_mock.call_args_list) == 3


@patch('requests.get')
@patch('notifications.services.send_telegram_msg')
@patch('notifications.async_email_sender.send_finish_fund_email.apply_async')
def test_notifications_subscribers_ok_without_id_chat(apply_async_mock, send_msg_mock, get_mock, post_notification):
    apply_async_mock.return_value = None
    fund_name = 'test'
    fake_responses_get = [Mock(), Mock()]

    fake_responses_get[0].json.return_value = {'id_chat': None, 'id_user': '123'}
    fake_responses_get[0].status_code = 200

    fake_responses_get[1].json.return_value = {'email': 'test@test.com'}
    fake_responses_get[1].status_code = 200

    get_mock.side_effect = fake_responses_get
    test_msg = TEST_MSG.copy()
    test_msg['percentage'] = 10.01
    test_msg['nombre_bot'] = 'Test'
    test_msg['cantidad_dias'] = 61
    test_msg['fecha_inicio'] = '15/01/2020 14:55:00'
    test_msg['nivel_de_riesgo'] = 'volume_profile_strategies_USDT'
    test_msg['perdida'] = 15
    test_msg['ganancia'] = 20

    response = post_notification(
        'notifications:subscriptor_notification',
        test_msg,
        {'fund_name': fund_name})

    get_mock.assert_called()
    assert response.json() == Subs.OK_MESSAGE_RESPONSE
    send_msg_mock.assert_not_called()


@patch('requests.get')
@patch('notifications.services.send_telegram_msg')
def test_notifications_subscribers_fund_not_found(send_msg_mock, get_mock, post_notification):
    fund_name = 'test'
    get_mock.return_value.status_code = 400
    response = post_notification(
        'notifications:subscriptor_notification',
        TEST_MSG,
        {'fund_name': fund_name})

    assert response.json()['error'] == 'fund not found'
    get_mock.assert_called()
    send_msg_mock.assert_not_called()


@patch('notifications.views.PushNotification')
def test_send_push_notification_ok(mock_push, client):
    data = {"title": "test", "message": "test"}

    response = client.post(reverse('notifications:send-push-notification'), data=data)

    assert response.status_code == status.HTTP_200_OK


def test_send_push_notification_invalid_data(client):
    data = {"title": "test"}

    response = client.post(reverse('notifications:send-push-notification'), data=data)

    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_toggle_user_notifications_enable(client, user_gcm_devices):
    user_gcm_devices(user_id=1)
    client.put(reverse('notifications:toggle-user-notifications', kwargs={'user_id': 1, 'active': True}))
    for active in GCMDevice.objects.filter(name='1').values_list('active', flat=True):
        assert active


@pytest.mark.django_db
def test_toggle_user_notifications_disable(client, user_gcm_devices):
    user_gcm_devices(user_id=1)
    client.put(reverse('notifications:toggle-user-notifications', kwargs={'user_id': 1, 'active': False}))
    for active in GCMDevice.objects.filter(name='1').values_list('active', flat=True):
        assert active is False
