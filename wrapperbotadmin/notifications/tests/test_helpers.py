import pytest
from notifications.helpers import MapRiskLevels


@pytest.mark.parametrize('profile, expected_values', [
    ['pro_USDT', {'risk': 'Alto', 'currency': 'USDT'}],
    ['classic_BTC', {'risk': 'Medio', 'currency': 'BTC'}],
    ['volume_profile_strategies_USDT', {'risk': 'Multiestrategia', 'currency': 'USDT'}],
])
def test_map_risk_level_to_user_language(profile, expected_values):
    map_risk_levels = MapRiskLevels()
    risk, currency = map_risk_levels.map(profile)
    assert risk == expected_values['risk']
    assert currency == expected_values['currency']
