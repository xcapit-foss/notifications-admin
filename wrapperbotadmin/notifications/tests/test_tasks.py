from smtplib import SMTPException
from unittest.mock import patch

import pytest
from celery.exceptions import Retry
from pytest import raises
import notifications
from notifications.tasks import send_validation_email, send_referral_email, send_reset_password_email, \
    send_finish_fund_email


@pytest.fixture
def log_mock(monkeypatch):
    return monkeypatch.setattr(notifications.tasks.logger, 'exception', lambda *args, **kwargs: None)


@patch('django.core.mail.EmailMultiAlternatives.send')
def test_send_validation_email(send_mock, validation_data):
    send_mock.return_value = None
    send_validation_email(validation_data)
    send_mock.assert_called()


@patch('notifications.tasks.send_validation_email.retry')
@patch('django.core.mail.EmailMultiAlternatives.send')
def test_send_validation_email_exception(send_mock, retry_mock, log_mock, validation_data):
    retry_mock.side_effect = Retry()
    send_mock.side_effect = SMTPException()
    with raises(Retry):
        send_validation_email(validation_data)
        log_mock.assert_called()
    send_mock.assert_called()
    retry_mock.assert_called()


@patch('django.core.mail.EmailMultiAlternatives.send')
def test_send_referral_email(send_mock, referral_data):
    send_mock.return_value = None
    send_referral_email(referral_data)
    send_mock.assert_called()


@patch('notifications.tasks.send_referral_email.retry')
@patch('django.core.mail.EmailMultiAlternatives.send')
def test_send_referral_email_exception(send_mock, retry_mock, log_mock, referral_data):
    retry_mock.side_effect = Retry()
    send_mock.side_effect = SMTPException()
    with raises(Retry):
        send_referral_email(referral_data)
        log_mock.assert_called()
    send_mock.assert_called()
    retry_mock.assert_called()


@patch('django.core.mail.EmailMultiAlternatives.send')
def test_send_reset_password_email(send_mock, validation_data):
    send_mock.return_value = None
    send_reset_password_email(validation_data)
    send_mock.assert_called()


@patch('notifications.tasks.send_reset_password_email.retry')
@patch('django.core.mail.EmailMultiAlternatives.send')
def test_send_reset_password_email_exception(send_mock, retry_mock, log_mock, validation_data):
    retry_mock.side_effect = Retry()
    send_mock.side_effect = SMTPException()
    with raises(Retry):
        send_reset_password_email(validation_data)
        log_mock.assert_called()
    send_mock.assert_called()
    retry_mock.assert_called()


@patch('django.core.mail.EmailMultiAlternatives.send')
def test_send_finish_fund_email(send_mock, finish_fund_data):
    send_mock.return_value = None
    send_finish_fund_email(finish_fund_data)
    send_mock.assert_called()


@patch('django.core.mail.EmailMultiAlternatives.send')
def test_send_finish_fund_email_negative_percentage(send_mock, finish_fund_data_negative):
    send_mock.return_value = None
    send_finish_fund_email(finish_fund_data_negative)
    send_mock.assert_called()


@patch('notifications.tasks.send_finish_fund_email.retry')
@patch('django.core.mail.EmailMultiAlternatives.send')
def test_send_finish_fund_email_exception(send_mock, retry_mock, log_mock, finish_fund_data):
    retry_mock.side_effect = Retry()
    send_mock.side_effect = SMTPException()
    with raises(Retry):
        send_finish_fund_email(finish_fund_data)
        log_mock.assert_called()
    send_mock.assert_called()
    retry_mock.assert_called()
