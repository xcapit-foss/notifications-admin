from abc import ABC, abstractmethod
from push_notifications.models import GCMDevice as GCMDeviceModel


class Device(ABC):

    @abstractmethod
    def send_message(self, message: str, **kwargs):
        """"""


class FCMDevice(Device):

    def __init__(self, device_model: GCMDeviceModel):
        self._device_model = device_model

    def send_message(self, message: str, **kwargs):
        return self._device_model.send_message(message=message, **kwargs)


class FakeDevice(Device):

    def send_message(self, message: str, **kwargs):
        return True
