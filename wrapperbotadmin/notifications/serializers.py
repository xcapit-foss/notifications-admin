from rest_framework import serializers
from .models import Notifications


class EmailValidationSerializer(serializers.Serializer):
    email = serializers.CharField(max_length=256, required=True, allow_blank=False, allow_null=False)
    uid = serializers.CharField(max_length=1000, required=True, allow_blank=False, allow_null=False)
    token = serializers.CharField(max_length=1000, required=True, allow_blank=False, allow_null=False)
    domain = serializers.CharField(max_length=1000, required=True, allow_blank=False, allow_null=False)

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass


class ReferralEmailSerializer(serializers.Serializer):
    referral_email = serializers.CharField(max_length=256, required=True, allow_blank=False, allow_null=False)
    user_email = serializers.CharField(max_length=256, required=True, allow_blank=False, allow_null=False)
    encode_email = serializers.CharField(max_length=256, required=True, allow_blank=False, allow_null=False)
    referral_code = serializers.CharField(max_length=1000, required=True, allow_blank=False, allow_null=False)
    domain = serializers.CharField(max_length=1000, required=True, allow_blank=False, allow_null=False)

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass


class NotificationsSerializer(serializers.Serializer):
    created = serializers.DateTimeField(required=False)
    read = serializers.BooleanField(default=False)
    user_id = serializers.CharField(max_length=100, required=False, allow_blank=True, allow_null=True)
    type = serializers.CharField(max_length=50, required=True, allow_blank=False, allow_null=False)
    title = serializers.CharField(max_length=100, required=True, allow_blank=False, allow_null=False)
    message = serializers.CharField(max_length=400, required=True, allow_blank=False, allow_null=False)

    def create(self, validated_data):
        return Notifications.objects.create(**validated_data)

    def update(self, instance, validated_data):
        return instance.save()


class SubscriberNotificationSerializer(serializers.Serializer):
    message = serializers.CharField(max_length=400, required=True, allow_blank=False, allow_null=False)
    nivel_de_riesgo = serializers.CharField(max_length=200, required=False, allow_blank=True, allow_null=True)
    nombre_bot = serializers.CharField(max_length=100, required=False, allow_blank=True, allow_null=True)
    id_corrida = serializers.IntegerField(required=False, allow_null=True)
    fecha_inicio = serializers.CharField(max_length=200, required=False, allow_blank=True, allow_null=True)
    ganancia = serializers.IntegerField(required=False, allow_null=True)
    perdida = serializers.IntegerField(required=False,  allow_null=True)
    cantidad_dias = serializers.IntegerField(required=False, allow_null=False)
    estado = serializers.CharField(max_length=200, required=False, allow_blank=True, allow_null=True)
    fecha_fin = serializers.CharField(max_length=200, required=False, allow_blank=True, allow_null=True)
    percentage = serializers.DecimalField(max_digits=17, decimal_places=2, required=False, allow_null=True)

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass


class PushNotificationsSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=100, required=True, allow_blank=False, allow_null=False)
    message = serializers.CharField(max_length=400, required=True, allow_blank=False, allow_null=False)

    def create(self, validated_data):
        """"""

    def update(self, instance, validated_data):
        """"""
