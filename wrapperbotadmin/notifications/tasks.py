import logging
from wrapperbotadmin.celery import app
from notifications.clients import DjangoEmailClient


logger = logging.getLogger(__name__)
SEND_EMAIL_EXCEPTION_MSG = 'Async send email exception. Data: {}'


@app.task(bind=True, name='notifications.tasks.send_validation_email')
def send_validation_email(self, data: dict):
    try:
        DjangoEmailClient().send_email_validation(data)
    except Exception as exc:
        logger.exception(SEND_EMAIL_EXCEPTION_MSG.format(str(data)))
        raise self.retry(exc=exc, countdown=60)


@app.task(bind=True, name='notifications.tasks.send_referral_email')
def send_referral_email(self, data: dict):
    try:
        DjangoEmailClient().send_referral_email(data)
    except Exception as exc:
        logger.exception(SEND_EMAIL_EXCEPTION_MSG.format(str(data)))
        raise self.retry(exc=exc, countdown=60)


@app.task(bind=True, name='notifications.tasks.send_reset_password_email')
def send_reset_password_email(self, data: dict):
    try:
        DjangoEmailClient().send_reset_password_email(data)
    except Exception as exc:
        logger.exception(SEND_EMAIL_EXCEPTION_MSG.format(str(data)))
        raise self.retry(exc=exc, countdown=60)


@app.task(bind=True, name='notifications.tasks.send_finish_fund_email')
def send_finish_fund_email(self, data: dict):
    try:
        DjangoEmailClient().send_finish_fund_email(data)
    except Exception as exc:
        logger.exception(SEND_EMAIL_EXCEPTION_MSG.format(str(data)))
        raise self.retry(exc=exc, countdown=60)
