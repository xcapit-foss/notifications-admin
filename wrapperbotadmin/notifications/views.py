import os
import requests
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from push_notifications.models import GCMDevice
from core.notifications.channels import PushChannel
from core.notifications.notifications import PushNotification
from notifications.helpers import send_telegram_msg
from .async_email_sender import AsyncValidationEmailSender, AsyncReferralEmailSender, AsyncResetPasswordEmailSender
from .devices import FCMDevice
from .serializers import EmailValidationSerializer, ReferralEmailSerializer, NotificationsSerializer, \
    SubscriberNotificationSerializer, PushNotificationsSerializer
from .models import Notifications
from .services import SubscriberNotificationService, FundNotExistsException
import logging

logger = logging.getLogger(__name__)
ERROR_MSG_RESPONSE = {"error": "message needed"}


class Dev(APIView):
    OK_MESSAGE_RESPONSE = {"status": "ok"}

    devs = os.environ.get('DEVS_CHAT_ID', "[{'nombre': 'Maxi', 'chat_id': 844938958}]")

    send_msg = send_telegram_msg

    def post(self, request, *args, **kwargs):
        body = request.data
        if "message" not in body:
            return Response(ERROR_MSG_RESPONSE)

        for i in eval(self.devs):
            Dev.send_msg(body["message"], i["chat_id"], os.environ.get("TG_TOKEN_ADMIN"))

        return Response(self.OK_MESSAGE_RESPONSE)


class Administrative(APIView):
    OK_MESSAGE_RESPONSE = "todo bien"

    group_chat_id = os.environ.get("ADMINS_CHAT_GROUP_ID")

    send_msg = send_telegram_msg

    def post(self, request, *args, **kwargs):
        body = request.data

        if "message" not in body:
            return Response(ERROR_MSG_RESPONSE)

        Administrative.send_msg(body.get("message"), self.group_chat_id, os.environ.get("TG_TOKEN_ADMIN"))

        return Response(self.OK_MESSAGE_RESPONSE)


class Subs(APIView):
    OK_MESSAGE_RESPONSE = "todo bien"
    serializer_class = SubscriberNotificationSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if not serializer.is_valid(raise_exception=False):
            return Response(ERROR_MSG_RESPONSE, status=status.HTTP_400_BAD_REQUEST)

        subs_service = SubscriberNotificationService(serializer.validated_data, kwargs.get('fund_name'))
        try:
            subs_service.execute()
        except FundNotExistsException:
            logger.exception('Fund not exists on send notification')
            return Response({'error': subs_service.error}, status=status.HTTP_400_BAD_REQUEST)

        return Response(self.OK_MESSAGE_RESPONSE, status=status.HTTP_200_OK)


class SendEmailValidationAPIView(APIView):
    serializer_class = EmailValidationSerializer

    def post(self, request):
        ser = self.serializer_class(data=request.data)
        if not ser.is_valid():
            return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)
        async_email_sender = AsyncValidationEmailSender(ser.data)
        async_email_sender.execute()
        return Response({'sent': True}, status=status.HTTP_200_OK)


class SendReferralEmailAPIView(APIView):
    serializer_class = ReferralEmailSerializer

    def post(self, request):
        ser = self.serializer_class(data=request.data)
        if not ser.is_valid():
            return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)
        async_email_sender = AsyncReferralEmailSender(ser.data)
        async_email_sender.execute()
        return Response({'sent': True}, status.HTTP_200_OK)


class SendEmailResetPasswordAPIView(APIView):
    serializer_class = EmailValidationSerializer

    def post(self, request):
        ser = self.serializer_class(data=request.data)
        if not ser.is_valid():
            return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)
        async_email_sender = AsyncResetPasswordEmailSender(ser.data)
        async_email_sender.execute()
        return Response({'sent': True}, status=status.HTTP_200_OK)


class UserNotificationsAPIView(APIView):
    serializer_class = NotificationsSerializer
    _notification_model = Notifications

    def get(self, request, user_id):
        notifications = Notifications.objects.filter(
            user_id=user_id
        ).order_by(
            '-created'
        )

        result = NotificationsSerializer(notifications, many=True).data

        return Response(result, status=status.HTTP_200_OK)

    def put(self, request, user_id):
        notifications = Notifications.objects.filter(
            user_id=user_id,
            read=False
        ).order_by(
            '-created'
        )

        if notifications.count() > 0:
            result = notifications.update(read=True)
            if result:
                return Response({}, status=status.HTTP_200_OK)
            else:
                return Response({'error': 'No se ha podido realizar la actualización'},
                                status=status.HTTP_400_BAD_REQUEST)

        return Response({'status': 'No hay registros a actualizar'}, status=status.HTTP_200_OK)


class CountUserNotificationsAPIView(APIView):
    serializer_class = NotificationsSerializer
    _notification_model = Notifications

    def get(self, request, user_id):
        notifications = Notifications.objects.filter(
            user_id=user_id,
            read=False
        ).order_by(
            '-created'
        ).count()

        return Response({'count': notifications}, status=status.HTTP_200_OK)


class CreateUserNotificationAPIView(APIView):
    api_app = os.environ.get("API_APP")
    serializer_class = NotificationsSerializer
    _notification_model = Notifications

    def post(self, request):
        data = request.data

        res_user = requests.get(f'{self.api_app}users/{data["user_id"]}')

        if res_user.status_code != 200:
            return Response({"error": "Usuario inexistente"}, status=status.HTTP_400_BAD_REQUEST)

        serializer = self.serializer_class(data=data, many=False)
        serializer.is_valid(raise_exception=True)
        result = serializer.save()

        if result:
            PushNotification(
                message=result.message,
                channel=PushChannel(
                    device=FCMDevice(device_model=GCMDevice.objects.filter(name=result.user_id))),
                title=result.title
            ).send()
            return Response({}, status=status.HTTP_201_CREATED)
        else:
            return Response({"error": "No se guardo correctamente"}, status=status.HTTP_400_BAD_REQUEST)


class SendPushNotificationsView(APIView):
    serializer_class = PushNotificationsSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            PushNotification(
                message=serializer.data.get('message'),
                channel=PushChannel(
                    device=FCMDevice(device_model=GCMDevice.objects.all())),
                title=serializer.data.get('title')
            ).send()
        return Response({}, status.HTTP_200_OK)


class ToggleUserNotificationsView(APIView):
    def put(self, request, user_id: int, active: bool):
        GCMDevice.objects.filter(name=str(user_id)).update(active=active)
        return Response({'user_id': user_id, 'active': active}, status=status.HTTP_200_OK)
