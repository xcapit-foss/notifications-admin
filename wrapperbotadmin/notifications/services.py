from notifications.async_email_sender import AsyncFinishFundEmailSender
from notifications.clients import ApiBotClient, ApiAppClient
from notifications.helpers import send_telegram_msg, MapRiskLevels
from wrapperbotadmin.settings import TG_TOKEN


class FundNotExistsException(Exception):
    pass


class SubscriberNotificationService:
    OK_MESSAGE_RESPONSE = "todo bien"
    ERROR_MSG_RESPONSE = 'fund not found'

    _tg_token = TG_TOKEN

    def __init__(self, data: dict, fund_name: str):
        self._data = data
        self._fund_name = fund_name
        self._api_bot_client = ApiBotClient()
        self._api_app_client = ApiAppClient()
        self._send_msg = send_telegram_msg
        self._map_risk_levels = MapRiskLevels()
        self._first_subscriber = None
        self._user = None
        self.error = None

    def _check_first_subscriber(self):
        if not self._first_subscriber:
            self.error = self.ERROR_MSG_RESPONSE
            raise FundNotExistsException('Fund has no subscribers')

    def _get_first_subscriber(self):
        first_subscriber = self._api_bot_client.get_first_subscriber(self._fund_name)
        self._first_subscriber = first_subscriber

    def _id_chat_exists(self):
        return self._first_subscriber['id_chat'] != '' and self._first_subscriber['id_chat'] is not None

    def _send_telegram_msg(self):
        if self._id_chat_exists():
            self._send_msg(self._data['message'], self._first_subscriber['id_chat'], self._tg_token)

    def _id_user_exists(self):
        return self._first_subscriber['id_user'] != '' and self._first_subscriber['id_user'] is not None

    def _get_user(self):
        self._user = self._api_app_client.get_user(self._first_subscriber['id_user'])

    def _prepare_email_data(self):
        risk_level = self._map_risk_levels.map(self._data['nivel_de_riesgo'])
        self._data['nivel_de_riesgo'] = risk_level.risk
        self._data['currency'] = risk_level.currency
        self._data['email'] = self._user['email']

    def _send_async_email(self):
        async_email_sender = AsyncFinishFundEmailSender(self._data)
        async_email_sender.execute()

    def _send_email(self):
        if self._id_user_exists():
            self._get_user()
            self._prepare_email_data()
            self._send_async_email()

    def execute(self):
        self._get_first_subscriber()
        self._check_first_subscriber()
        self._send_telegram_msg()
        self._send_email()

