from abc import ABC
from .tasks import send_validation_email, send_referral_email, send_reset_password_email, send_finish_fund_email
import random


class AsyncEmailSender(ABC):
    _default_queue = 'default_emails_queue'
    _task = None

    def __init__(self, data: dict):
        self._data = data

    @staticmethod
    def _generate_random_countdown():
        return random.random()

    def execute(self):
        self._task.apply_async(
            queue=self._default_queue,
            countdown=self._generate_random_countdown(),
            serializer='json',
            retry=True,
            max_retries=2,
            kwargs={'data': self._data}
        )


class AsyncValidationEmailSender(AsyncEmailSender):
    _task = send_validation_email


class AsyncReferralEmailSender(AsyncEmailSender):
    _task = send_referral_email


class AsyncResetPasswordEmailSender(AsyncEmailSender):
    _task = send_reset_password_email


class AsyncFinishFundEmailSender(AsyncEmailSender):
    _task = send_finish_fund_email
