from django.db import models


class Notifications(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    read = models.BooleanField(default=False)
    user_id = models.CharField(max_length=100, null=True)
    type = models.CharField(max_length=50)
    title = models.CharField(max_length=100)
    message = models.CharField(max_length=400)
