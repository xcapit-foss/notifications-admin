import os
import requests
import collections as col


class MapRiskLevels:
    def __init__(self):
        self.RISK_LEVELS = {"classic": "Medio", "pro": "Alto", "volume_profile_strategies": "Multiestrategia"}
        self.profile = ''
        self.risk = ''
        self.currency = ''
        self.RiskLevel = col.namedtuple('RiskLevel', ['risk', 'currency'])

    def map(self, profile):
        self.profile = profile
        self.split_risk_and_currency()
        return self.RiskLevel(self.risk, self.currency)

    def split_risk_and_currency(self):
        risk_level, currency = self.profile.rsplit("_", 1)
        if risk_level and currency and risk_level in self.RISK_LEVELS:
            self.risk = self.RISK_LEVELS[risk_level]
            self.currency = currency


def send_telegram_msg(message, chat_id, bot_token):
    """Envía un mensaje al chat de telegram que se indica
    en la variable de entorno TELEGRAM_CHAT_ID con el bot
    con token especificado en la var de entorno TELEGRAM_BOT_TOKEN.

    Parameters
    ----------
    message : str
        Mensaje a enviar por telegram, by default ''
    chat_id : str
    bot_token : str
    """
    telegram_api = os.getenv('TELEGRAM_API')
    url = f'{telegram_api}/bot{bot_token}/sendMessage?chat_id={chat_id}&text={message}'
    response = requests.post(url, timeout=15)
    if response.status_code != 200:
        raise Exception(f'Telegram message failed: {response.__dict__}')
