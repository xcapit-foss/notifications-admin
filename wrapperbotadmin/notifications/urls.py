from django.urls import path
from push_notifications.api.rest_framework import GCMDeviceViewSet
from .views import Dev, Subs, Administrative, SendEmailValidationAPIView, SendReferralEmailAPIView, \
    SendEmailResetPasswordAPIView, UserNotificationsAPIView, CreateUserNotificationAPIView, \
    CountUserNotificationsAPIView, SendPushNotificationsView, ToggleUserNotificationsView

app_name = "notifications"

urlpatterns = [
    path('devs', Dev.as_view(), name='dev_notification'),
    path("subscriptors/fund_name/<fund_name>", Subs.as_view(), name='subscriptor_notification'),
    path('administrative', Administrative.as_view(), name='admin_notification'),
    path('send-email-validation/', SendEmailValidationAPIView.as_view(), name='send-email-validation'),
    path('send-referral-email/', SendReferralEmailAPIView.as_view(), name='send-referral-email'),
    path('send-email-reset-password/', SendEmailResetPasswordAPIView.as_view(), name='send-email-reset-password'),
    path('user/<user_id>', UserNotificationsAPIView.as_view(), name='user-notifications'),
    path('create', CreateUserNotificationAPIView.as_view(), name='create-notification'),
    path('user/<user_id>/count', CountUserNotificationsAPIView.as_view(), name='count-notifications'),
    path('user/<user_id>/toggle/<active>/', ToggleUserNotificationsView.as_view(), name='toggle-user-notifications'),
    path('device/fcm', GCMDeviceViewSet.as_view({'post': 'create'}), name='create-fcm-device'),
    path('send-push-notifications/', SendPushNotificationsView.as_view(), name='send-push-notification')
]
