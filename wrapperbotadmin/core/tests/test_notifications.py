from unittest.mock import Mock
from core.notifications.channels import Channel, PushChannel
from core.notifications.notifications import PushNotification
from notifications.devices import FakeDevice


def test_new_push_notification():
    assert PushNotification(message="", channel=Mock(spec=Channel))


def test_push_notification_send():
    notification = PushNotification(
        message="some text",
        channel=PushChannel(FakeDevice())
    )

    assert notification.send()
