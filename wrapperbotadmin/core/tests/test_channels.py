from core.notifications.channels import PushChannel
from notifications.devices import FakeDevice


def test_new():
    assert PushChannel(FakeDevice())


def test_send():
    assert PushChannel(FakeDevice()).send(message="some text") is True
