from abc import ABC, abstractmethod

from core.notifications.channels import Channel


class Notification(ABC):

    @abstractmethod
    def send(self, *args, **kwargs):
        """"""


class PushNotification(Notification):

    def __init__(self, message: str, channel: Channel, **kwargs):
        self._channel = channel
        self._message = message
        self._kwargs = kwargs

    def send(self, *args, **kwargs):
        return self._channel.send(message=self._message, **self._kwargs)
