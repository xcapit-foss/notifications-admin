from abc import ABC, abstractmethod
from notifications.devices import Device


class Channel(ABC):

    @abstractmethod
    def send(self, *args, **kwargs):
        """"""


class PushChannel(Channel):

    def __init__(self, device: Device):
        self._device = device

    def send(self, message: str, **kwargs):
        return self._device.send_message(message, **kwargs)
